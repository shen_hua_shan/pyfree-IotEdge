#!/bin/bash
#@pyfree 2020-04-16,py8105@163.com
#创建程序目录
#run for root
#定义程序全局路径变量
#程序根目录
dir='/usr/local/monitor'
curdir=$(pwd)

if [ ! -d $dir ]; then
    mkdir -p $dir
fi
#
echo "appdir="$dir
echo "curdir="$curdir
#复制程序文件到程序目录
/bin/cp -f {pyfree-monitor,SWL,*.xml} $dir
#
/bin/cp -f {libmosquitto.so.1,libmosquittopp.so.1} /usr/local/lib
ln -sf /usr/local/lib/libmosquitto.so.1 /usr/local/lib/libmosquitto.so
ln -sf /usr/local/lib/libmosquittopp.so.1 /usr/local/lib/libmosquittopp.so
if [ ! -f "/etc/ld.so.conf.d/mosquitto_lib.conf" ];then
#添加程序动态库到系统
cat > /etc/ld.so.conf.d/mosquitto_lib.conf <<eof
/usr/local/lib
eof
#重新加载动态库，使其生效
ldconfig
fi
#
ls $dir
#
if [ ! -f "$dir/monitorStart.sh" ];then
#创建程序启动脚本
cat > $dir/monitorStart.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-monitor*" | grep -v "grep" | wc -l\`
if [ \$pid -eq 0 ];then
    cd $dir
	echo "The monitor server will be start"
    nohup ./pyfree-monitor & > /dev/null
else
    echo "The monitor server is alreadly running"
fi 
eof
fi

if [ ! -f "$dir/monitorStop.sh" ];then
#创建程序关闭脚本
cat > $dir/monitorStop.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-monitor*" | grep -v "grep" | wc -l\`
if [ \$pid -ne 0 ];then
	echo "The monitor server will be stop"
    killall -9 pyfree-monitor
else
	echo "The monitor server is stoping, don't stop it"
fi 
eof
fi

#给服务启动脚本添加执行权限
chmod o+x $dir/{monitorStart.sh,monitorStop.sh}

if [ ! -f "/usr/lib/systemd/system/pyfreeMonitor.service" ];then
#创建程序启动文件到centos7服务启动路径/usr/lib/systemd/system
cat > /usr/lib/systemd/system/pyfreeMonitor.service <<eof
[Unit]
Description=pyfree-monitor
After=syslog.target network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
ExecStart=$dir/monitorStart.sh
ExecReload=
ExecStop=$dir/monitorStop.sh
PrivateTmp=true
[Install]
WantedBy=multi-user.target
eof
fi

#生成liscense文件sn.txt, 如果是32位系统，修改为SWL_x32.exe 
#0表示网卡，1表示磁盘； 22表示生成sn.txt文件，这里选择网卡，生成sn.txt liscense文件
cd $dir
./SWL 0 22
cd $curdir

#设置开机启动服务
systemctl daemon-reload
chmod o-x /usr/lib/systemd/system/pyfreeMonitor.service
systemctl enable pyfreeMonitor.service
#查看服务是否开机启动：
#systemctl is-enabled pyfreeMonitor.service
#设置开机时禁用服务
#systemctl disable pyfreeMonitor.service
#启动pyfreeMonitor服务
#systemctl start pyfreeMonitor.service

#停止pyfreeMonitor服务
#systemctl stop pyfreeMonitor.service

