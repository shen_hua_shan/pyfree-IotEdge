﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:pyfree项目
--脚本版本:1.0
-----------------------------------------------------------------------------------------------------------------------------------------
function getTotalCallCMD()
	local allcall = ""
	return allcall,0
end

function DEC_HEX(IN)
    local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
    while IN>0 do
        I=I+1
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

function num2hex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

function getDownControl(exetype, ptype, addr, val)
	--collectgarbage("collect")
	local controlCmd = "NULL"
	local check = 2
	print(string.format("%d,%d,%d,%.3f",exetype,ptype,addr,val))
	--print("..1..")
	if 1==exetype then
		--print("..1..")
		controlCmd = ""
	elseif 2==exetype then
		--print("..2..")
		local intStr = string.format("%08X",addr)
		--print(intStr)
		local valStr = string.format("%08X",val)
		--print(valStr)
		local intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		local valStr_ = string.sub( valStr, 7, 8 )..string.sub( valStr, 5, 6 )..string.sub( valStr, 3, 4 )..string.sub( valStr, 1, 2 )
		controlCmd = "F3".."0B00"..intStr_..valStr_.."FF"
		--print(controlCmd)
	else 
		--print("..3..")
		local lenStr = string.format("%04X",12)
		--print(lenStr)
		local intStr = string.format("%08X",addr)
		--print(intStr)
		local ptypeStr = string.format("%02X",ptype)
		--print(ptypeStr)
		local valStr = string.format("%08X",val)
		--print(valStr)
		local lenStr_ = string.sub( lenStr, 3, 4 )..string.sub( lenStr, 1, 2 )
		local intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		local valStr_ = string.sub( valStr, 7, 8 )..string.sub( valStr, 5, 6 )..string.sub( valStr, 3, 4 )..string.sub( valStr, 1, 2 )
		controlCmd = "F7"..lenStr_..intStr_..ptypeStr..valStr_.."FF"
		print(controlCmd)
	end
	--print("..4..")
	return controlCmd,check
end

function hex2string(hex)
	local str, n = hex:gsub("(%x%x)[ ]?", function (word)
	return string.char(tonumber(word, 16))
	end)
	return str
end

function getReciveIDVAL(up_cmd,down_cmd)
	local stra = _cmd
	--print(stra)
	local recs = ""
	local exeType = 0
	local pType = 0
	--print("...1...\n")
	if string.sub( stra, 1, 1 )~="F" then
		return recs,0
	end
	if string.sub( stra, 1, 2 )~="F5" then
		exeType = 1
	end
	if string.sub( stra, 1, 2 )~="F6" then
		exeType = 2
	end
	--print("...2...\n")
	if string.len(stra)<13 then
		return recs,0
	end
	--print("...3...\n")
	local lLen = string.sub( stra, 3, 4 )
	local hLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)+16*tonumber("0x"..hLen)
	--print(dlen)
	if dlen<=0 then
		return recs,0
	end
	--print("...4...\n")
	local size = 0;
	local id04 = string.sub( stra, 7, 8 )
	local id03 = string.sub( stra, 9, 10 )
	local id02 = string.sub( stra, 11, 12 )
	local id01 = string.sub( stra, 13, 14 )
	--print(id04..id03..id02..id01)
	local idx = tonumber("0X"..id04)+16*tonumber("0X"..id03)+256*tonumber("0X"..id02)+4096*tonumber("0X"..id01)
	--print(idx)	
	--print("...5...\n")
	local ptypeStr = string.sub( stra, 15, 16 )
	local pType = tonumber("0x"..ptypeStr)
	--print("...5...\n")
	
	local val04 = string.sub( stra, 17, 18 )
	local val03 = string.sub( stra, 19, 20 )
	local val02 = string.sub( stra, 21, 22 )
	local val01 = string.sub( stra, 23, 24 )
	--print(val04..val03..val02..val01)
	local val = tonumber("0X"..val04)+16*tonumber("0X"..val03)+256*tonumber("0X"..val02)+4096*tonumber("0X"..val01)

	local valmap = string.format("%d,%d,%d,%d",idx,val,pType,exeType)
	--print(valmap)
	recs = recs..valmap
	size = 1

	--print("...6...\n")
	return recs,size
end
