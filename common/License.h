#ifndef __LICENSE_H__
#define __LICENSE_H__
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : License.hLICENSE
  *File Mark       : 
  *Summary         : 
  *简易的程序加密类,根据网卡或磁盘信息进行加密,是的程序使用与硬件绑定,防止程序乱拷贝使用
  *清注意使用该类会使程序需要一定的系统用户权限
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
// ANSC C/C++
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

/**
* @author 
* Create: 2009/09/11
* Modify: 2018/03/27
* Modify: 2020/06/27 windows(CRLF)格式指定
*/

#define MAX_LICENSE_SIZE 32

class CLicense
{
public:
	CLicense();
	CLicense(const CLicense& other);
	bool operator !=(const CLicense& other);
	bool operator ==(const CLicense& other);

	/**
	 * 获取网卡或磁盘信息并写入本地缓存szMacAddress
	 * @return {bool} 获取是否成功,即返回变量source_flag
	 */
	bool Create();
	/**
	 * 根据网卡或磁盘信息进行加密并写入m_szLicense
	 * @return {bool} 是否成功
	 */
	bool encrypt();
	/**
	 * 根据指定文件进行网卡或此片的序列化信息读取或保存
	 * @param strFile {char*} 文件名
	 * @param bStoring {bool} 读取或写入
	 * @return {bool} 是否成功
	 */
	bool SerializeSource(const char* strFile, bool bStoring);
	/**
	 * 将缓存的网卡或磁盘信息转换为字符串输出
	 * @return {string} 硬件信息的字符串值
	 */
	std::string ToStringS() const;
	/**
	 * 根据指定文件进行加密信息读取或保存
	 * @param strFile {char*} 文件名
	 * @param bStoring {bool} 读取或写入
	 * @return {bool} 是否成功
	 */
	bool Serialize(const char* strFile, bool bStoring);
	/**
	 * 将缓存的加密信息转换为字符串输出
	 * @return {string} 加密信息的字符串值
	 */
	std::string ToString() const;
	// added 2010.11.15
	/**
	 * 设置取物理地址类型
	 * @param mMacType {int} 指定物理地址类型，
	 * mMacType=0,网卡信息;mMacType!=0，磁盘信息
	 */
	void SetMacAddrType(int mMacType){m_nMacType = mMacType;}
	/**
	 * 取硬盘序列码
	 * @param szSN {char*} 序列码存储地址
	 * @param n {int} 序列码存储大小
	 * @return {int} 实际序列码长度
	 */
	int GetHDSN(char * szSN, int n);
private:
	/**
	 * 根据指定字符标识划分字符串
	 * @param _strlist {verctor} 划分结果容器
	 * @param src {string} 被划分字符串
	 * @param div {string} 划分标识字符串
	 * @return {bool} 是否成功
	 */
	bool string_divide(std::vector<std::string> &_strlist, const std::string src, const std::string div);
protected:
	//磁盘或网卡信息是否成功获取
	bool source_flag;
	//磁盘信息缓存
	char szMacAddress[128];
	unsigned char byMacAddrLen;
	//网卡信息缓存
	unsigned char addr[6];
	//加密信息缓存
	char m_szLicense[MAX_LICENSE_SIZE];
	//物理地址类型
	int m_nMacType;
};

#endif  /*__SNFACTORY_H__*/
