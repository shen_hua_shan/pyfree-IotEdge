#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _APP_EXIT_IO_H_
#define _APP_EXIT_IO_H_

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : appexitio.h
  *File Mark       : 
  *Summary         : 
  *程序退出信号处理接口
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#ifdef WIN32
#include <windows.h> 
#else
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#endif

#ifdef WIN32
bool ctrlhandler(unsigned long fdwctrltype);
#else
class SignalHandler {
public:
	SignalHandler();

  void printf_out();
public:
	// 程序退出时的函数操作
	static void handle_signal(int n, siginfo_t *siginfo, void *myact);
};

#endif // WIN32

#endif
