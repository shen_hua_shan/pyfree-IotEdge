#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _DB_TYPE_DEF_H_
#define _DB_TYPE_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : dbtypedef.h
  *File Mark       : 
  *Summary         : 数据存储相关业务模型定义
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <list>
#include <string>
#include <time.h>

#ifdef __linux__
#include <stdio.h>
#define sprintf_s sprintf
#endif

struct SADataItem
{
    inline void ToQT()
    {
		long long ltt= (Time - 116444736000000000L) / 10000;
		time_t sec = ltt / 1000;
		unsigned int msec = ltt % 1000;
		struct tm stm;
#ifdef WIN32
		localtime_s(&stm, &sec);
#else
		localtime_r(&sec,&stm);
#endif // WIN32

		char bufT[64] = { 0 };
		sprintf_s(bufT,"%04d-%02d-%02d %02d:%02d:%02d.%03d"
			, stm.tm_year, stm.tm_mon, stm.tm_mday, stm.tm_hour, stm.tm_min, stm.tm_sec,msec);
		char bufVal[64] = { 0 };
		sprintf_s(bufVal, "%.3f", Value);
		val_STR = std::string(bufVal);
    };
	int			Dev_Index;
    int         YC_Index;
    long long   Time;
    float       Value;
    std::string     dt_STR;
    std::string     val_STR;
};

struct SAQueryConf
{
    SAQueryConf() : devid(-1), idx(-1),tvStart(0),tvStop(0),m_limit(-1)
    {

    };
	int devid;
    int idx;
    long long tvStart;
    long long tvStop;
    int m_limit;
};

struct LimitData
{
    float			minValue;
    std::string     min_dt_STR;
    float			maxValue;
    std::string     max_dt_STR;
};
#endif //SADEF_H
