#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RECORD_QUEUE_H_
#define _RECORD_QUEUE_H_
/***********************************************************************
  *Copyright 2017-06-27, Zhuhai Singyes Applied Materials Technology Co., Ltd.
  *
  *File Name       : record_queue.h
  *File Mark       :
  *Summary         : 记录数据缓存,单体类
  *maintain cache data for record info
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifdef __linux__
#include <string.h>
#include <string>
#endif
#include <queue>
#include <map>

#include "Mutex.h"
#include "dbtypedef.h"

class RecordQueue
{
public:
	static RecordQueue* getInstance();
	static void Destroy();
	~RecordQueue();
	//////////////////////////////////////////////////////////////
	/**
	 * 缓存添加记录信息
	 * @param devId {long } 设备编号
	 * @param _idx {int } 点编号
	 * @param _t {long long } 时间(毫秒)
	 * @param _val {float } 值
	 * @return {void}
	 */
	void addItem(long devId,int _idx, long long _t, float _val);
	/**
	 * 从缓存中读取一组数据
	 * @return {map} 数据集
	 */
	std::map<long,std::list<SADataItem> > getDatas();
private:
	RecordQueue() {
		init();
	};
	RecordQueue(const RecordQueue&);
	RecordQueue& operator=(const RecordQueue&) { return *this; };
	void init() {
	};
private:
	static RecordQueue* instance;
private:
	std::map<long,std::list<SADataItem> > _datas;	//缓存容器
	PYMutex data_mutex;								//线程锁			
};

#endif //_RecordQueue_H_
