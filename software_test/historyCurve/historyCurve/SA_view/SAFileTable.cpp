/*
**
*/
#include "SAFileTable.h"

#include <QApplication>
#include <QModelIndex>
#include <QMenu>
#include <QAction>
#include <QColor>
#include <QPalette>
#include <QHeaderView>
#include <QAbstractItemModel>
#include <QMouseEvent>
#include <QDir>
#include <QDebug>

#include "treemodel.h"
#include "plot/pfunc_qt.h"
#include "src/confdeal.h"

SAFileTableView::SAFileTableView(QAbstractItemModel * model,QWidget * parent)
    : MyTableView(model,parent)
{
    win_init();
}

SAFileTableView::~SAFileTableView()
{

};

void SAFileTableView::clear()
{
    this->recover();
};

void SAFileTableView::addFileToList(QString _name)
{
    TreeModel *model = qobject_cast<TreeModel *>(this->model());
    QVector<QVariant> Data;
    Data << _name;
    model->insertItemToParent(QModelIndex(),Data);
}

void SAFileTableView::win_init()
{
    ConfDeal *ptr_ConfDeal = ConfDeal::getInstance();
    m_dbDir = ptr_ConfDeal->getdbDir();
    QDir _mydir(m_dbDir);
    if(!_mydir.exists())
    {
        if (!_mydir.mkpath(m_dbDir)){
            qDebug() << "Can't create " + m_dbDir;
        }
    }
    this->setColumnWidth(0,240);
    m_iTimer = startTimer(60000);
    loadFileList();
}

void SAFileTableView::loadFileList()
{
    QDir dir(m_dbDir);
    if (!dir.exists())
        return ;
    qDebug() << dir.absolutePath();

    QStringList subDirs;
    dir.setFilter(QDir::Dirs|QDir::NoDotAndDotDot);
    QFileInfoList list = dir.entryInfoList();
    qDebug() << QString("list size=%1").arg(list.size());
    for (int i = 0; i < list.size(); i++)
    {
        QFileInfo fileInfo = list.at(i);
        if (fileInfo.isDir())
        {
            subDirs.append(fileInfo.fileName());
            qDebug()<<QString("add sub dir %1").arg(fileInfo.fileName());
        }else{
            
        }
    }
#ifdef WIN32
    QString div = "\\";
#else
    QString div = "/";
#endif
    QStringList filters;
    QString exName = "db";
    filters << ("*."+exName);
    for(int index=0; index<subDirs.size();index++)
    {
        QDir _dir(m_dbDir+div+subDirs.at(index));
        _dir.setNameFilters(filters);
        // _dir.setFilter(QDir::Files);
        _dir.setSorting(QDir::DirsFirst);
        QFileInfoList _list = _dir.entryInfoList();
        for (int i = 0; i < _list.size(); i++)
        {
            QFileInfo fileInfo = _list.at(i);
            if (fileInfo.isDir())
            {
                continue;
            }
            QString temp=fileInfo.suffix().toLower();
            if (temp==exName)
            {
                // qDebug()<<QString("add db file %1").arg(fileInfo.fileName());
                addFileToList(div+subDirs.at(index)+div+fileInfo.fileName());
            }
        }       
    }

}
void SAFileTableView::timerEvent(QTimerEvent * event)
{
    if (event->timerId()==m_iTimer)
    {
        loadFileList();
    }
}

void SAFileTableView::contextMenuEvent ( QContextMenuEvent* /*event*/ )
{
    // defaultContextMenuEvent(event);
}

void SAFileTableView::mouseDoubleClickEvent ( QMouseEvent * event )
{
    QModelIndex _index = indexAt(event->pos());
    if(_index.isValid())
    {
        QString _name = _index.data().toString();
        emit fileSelect(_name);
    }
};

void SAFileTableView::saveFile()
{

}
