#ifndef SATABLEVIEW_H
#define SATABLEVIEW_H

// #include <QMap>

#include "tableview.h"

#include "SA_db/SADef.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QContextMenuEvent;
class QMouseEvent;
class QWidget;
QT_END_NAMESPACE

// struct CVal
// {
// 	float value;
// 	int size;
// 	float maxVal;
// };

class SATableView : public MyTableView
{
	Q_OBJECT
public:
    SATableView(QAbstractItemModel * model,QWidget * parent = 0);
    ~SATableView();

    void clear();
    void createDataList(QList<SADataItem> _datas);
    void addDataToList(QList<SADataItem> _datas);
protected:
    void contextMenuEvent ( QContextMenuEvent * event );
private:
    void win_init();
signals:

public slots:
    void saveFile();
private slots:

private:
	// QMap<QString,QMap<QString,CVal> > cvals;
};

#endif // SATABLEVIEW_H
