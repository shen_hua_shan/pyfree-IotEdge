#ifndef FILEQT_H
#define FILEQT_H

#include <QVector>
#include <QString>
#include <QStringList>

class FileQt
{
public:
	static FileQt* getInstance();
	static void Destroy();
	~FileQt();

	bool writeListInfo(QVector<QString> list,const QString path);
	bool readListInfo(QVector<QVector<QString> > &list,const QString path,QString _div);
	bool readListInfo(QVector<QVector<QString> > &list,const QString path);
	bool readListInfo_dot(QVector<QVector<QString> > &list,const QString path);
	bool readListInfo(QVector<QString> &list,const QString path);
	void getAllFileName_dot(const QString directory, const QString extName, QStringList& fileNames);
	void getAllFileName(const QString& directory, const QString& extName, QStringList& fileNames);
	void getSubDir(const QString& directory, QStringList& subdirs);

	bool readToVectorXML(QString _file, QStringList elements, QVector<QString> &_lists);
	bool readToVectorXML(QString _file, QStringList elements, QVector<QVector<QString> > &_lists,QString _div=",");
#ifdef WIN32
	bool readToVectorXLS(QString _file, QVector<QVector<QString> > &_lists, int sheet = 1);
#endif
private:
	bool readToVector(QString _file, QVector<QVector<QString> > &_lists,QString _div="\\s+");
	bool readToVector(QString _file, QVector<QString> &_lists);
private:
	FileQt();
	FileQt(const FileQt& ) {} // copy constructor
	FileQt& operator=(const FileQt& ) { return *this; } // assignment operator
private:
	static FileQt* instance;
};

#endif //FILEQT_H
