/*
*/
#include "SADB.h"

#include <stdio.h>

SADB::SADB(void)
{
	m_bOpen = false;
}

SADB::~SADB(void)
{
	if(m_bOpen)
		Close();
}

bool SADB::Open(char *szFilePath)
{
	if(m_bOpen)
		Close();
	if(szFilePath == NULL)
	{
		printf("db:%s is NULL\n", szFilePath);
		return false;
	}

	try
	{
		std::string  m_strCurFileName = szFilePath;
		//MoveCurFileToBakDir(m_strCurFileName);
		m_bOpen = open(m_strCurFileName.c_str());
		if (!m_bOpen)
		{
			return m_bOpen;
		}
		std::string sql;
		execDML("begin transaction;");

		if(!tableExists("SA_Data"))
		{
			sql = "CREATE TABLE [SA_Data] (\
				[Dev_Index] Integer,\
				[YC_Index] Integer,\
				[YC_Time] Integer, \
				[YC_Val] FLOAT DEFAULT 0)";
			execDML(sql.c_str());
		}
		execDML("commit transaction;");
		printf("success open db:%s\n", szFilePath);
	}
	catch (CSQLiteException& e)
	{
		printf("\n\n%s\n\n",e.errorMessage ());
		m_bOpen = false;
	}
	return m_bOpen;
};

bool SADB::IsOpen()
{
	return m_bOpen;
};

void SADB::Close()
{
	//printf("\n\n%s | %s | line: %d | Func: %s ",__TIME__,__FILE__,__LINE__,"Close");
	m_bOpen = false;
	close();
};


bool SADB::getSADataQuery(QList<SADataItem> &_datas,QSet<int> &_devids
	,QSet<int> &_idxs,SAQueryConf _qconf)
{
	if (!IsOpen())
		return false;

	//config sql
	QList<QString> _condition;
	if (_qconf.devid>=0)
	{
		_condition.push_back(QString(" Dev_Index=%1 ").arg(_qconf.devid));
	}
	if (_qconf.idx>=0)
	{
		_condition.push_back(QString(" YC_Index=%1 ").arg(_qconf.idx));
	}
	if (_qconf.tvStart)
	{
		char sqlbuf[128] = {0};
#ifdef WIN32
		sprintf(sqlbuf," YC_Time>%I64d "
			, _qconf.tvStart);
#else
        sprintf(sqlbuf," YC_Time>%lld "
            , _qconf.tvStart);
#endif
		_condition.push_back(QString(sqlbuf));
	}
	if (_qconf.tvStop)
	{
		char sqlbuf[128] = {0};
#ifdef WIN32
		sprintf(sqlbuf," YC_Time<=%I64d "
			, _qconf.tvStop);
#else
        sprintf(sqlbuf," YC_Time<=%lld "
            , _qconf.tvStop);
#endif
		_condition.push_back(QString(sqlbuf));
	}
	QString sql = "SELECT Dev_Index,YC_Index,YC_Time,YC_Val FROM SA_Data";
	if(!_condition.empty()){
		sql+=" where ";
		QList<QString>::iterator it_end = _condition.end();
		it_end--;
		for(QList<QString>::iterator it=_condition.begin();it!=_condition.end();it++)
		{
			sql+=*it;
			if (it!=it_end)
			{
				sql+=" and ";
			}
		}
	}
	sql += " order by YC_Time Desc";
	if (_qconf.m_limit>0)
	{
		sql += QString(" LIMIT %1 ").arg(_qconf.m_limit);
	}
	//get data from table
	printf("SQL:%s\n", sql.toStdString().c_str());
	try{
		CSQLiteQuery query = execQuery(sql.toStdString().c_str());
		while(!query.eof())
		{
			SADataItem item;
			item.Dev_Index = query.getInt64Field(0);
			item.YC_Index = query.getInt64Field(1);
			item.Time = query.getInt64Field(2);
			item.Value = query.getFloatField(3);
			item.ToQT();
			_datas.push_back(item);
			_devids.insert(item.Dev_Index);
			_idxs.insert(item.YC_Index);
			query.nextRow();
		}
	}catch (CSQLiteException& e)
	{
		printf("GetSAData FROM SA_Data TABLE ERROR: %s \n", e.errorMessage());
		return false;
	}
	return !_datas.empty();
}

long long SADB::GetLastFT(void)
{
	long long llLast = 0;
	if(m_bOpen)
	{
		CSQLiteQuery query = execQuery("select YC_Time from SA_Data order by YC_Time DESC limit 1");
		if(!query.eof())
			llLast = query.getInt64Field(0);
	}
	return llLast;
};



bool SADB::AddYARecorder(QList<SADataItem> &_datas)
{
	bool bRet = true;
	try {
		execDML("BEGIN TRANSACTION");
		for (QList<SADataItem>::iterator it = _datas.begin();
			it != _datas.end(); it++)
		{
			CSQLiteStatement stmt = compileStatement("INSERT INTO SA_Data(Dev_Index,YC_Index,YC_Time,YC_Val) VALUES (?,?,?,?)");
			stmt.bind(1, (*it).Dev_Index);
			stmt.bind(2, (*it).YC_Index);
			stmt.bind(3, (*it).Time);
			stmt.bind(4, (*it).Value);
			stmt.execDML();
		}
		bRet = (execDML("COMMIT TRANSACTION") == 1);
	}catch (CSQLiteException& e)
	{
		execDML("ROLLBACK TRANSACTION");
		printf("Save Raw Data Failed %s\n", e.errorMessage());
		bRet = false;
	}

	return bRet;
};
