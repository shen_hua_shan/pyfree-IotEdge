#include "CloseCheck.h"

#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QFormLayout>
#include <QMessageBox>
#include <QRegExp>
#include <QRegExpValidator>
#include <QTime>

CloseCheck::CloseCheck(QWidget * parent, Qt::WindowFlags f ) :
    QDialog(parent,f)
{
    this->setWindowTitle(tr("App close to confirm"));

    mQGridLayout = new QGridLayout(this);

    welcomeLabel = new QLabel(tr("Systerm is runing,do you want to close App now? \n")+
		tr(" Select OK is close App or select Cancel is unclose App. \n"));//+
		// tr(" If close to be decided, pelease imput validation, then check OK button!"));//2015-08-07 delete password ensure quit
    //welcomeLabel->setAlignment(Qt::AlignHCenter);
    mQGridLayout->addWidget(welcomeLabel, 0, 0, 1, 3);

    mQFormLayout = new QFormLayout();
    userLabel = new QLabel(tr("random text produced by computer:"));
    // mQFormLayout->setWidget(0, QFormLayout::LabelRole, userLabel);//2015-08-07 delete password ensure quit

    randomLineEdit = new QLineEdit();
	randomLineEdit->setReadOnly(true);
	randomLineEdit->clear();
	qsrand(QTime::currentTime().second());//init rand() function with current time
	this->setRandomText();
    // mQFormLayout->setWidget(0, QFormLayout::FieldRole, randomLineEdit);//2015-08-07 delete password ensure quit

    secretLabel = new QLabel(tr("validation text imputed by user:"));
    // mQFormLayout->setWidget(1, QFormLayout::LabelRole, secretLabel);//2015-08-07 delete password ensure quit

    imputLineEdit = new QLineEdit();
	QRegExp rx("[0-9]{6,6}");//limit the imput is 6bit 0-9 str
    QRegExpValidator v(rx,0);
	imputLineEdit ->setValidator(&v);
	//imputLineEdit ->setValidator(new QIntValidator(000000, 999999, this));
	imputLineEdit->clear();
    imputLineEdit->setEchoMode(QLineEdit::Password);
    // mQFormLayout->setWidget(1, QFormLayout::FieldRole, imputLineEdit);//2015-08-07 delete password ensure quit

    mQGridLayout->addLayout(mQFormLayout, 1, 0, 1, 3);

    okButton = new QPushButton(tr("OK"));
    mQGridLayout->addWidget(okButton, 2, 1, 1, 1);
    connect(okButton, SIGNAL(clicked()), this, SLOT(isOK()));

    canleButton = new QPushButton(tr("Cancel"));
    mQGridLayout->addWidget(canleButton, 2, 2, 1, 1);
    connect(canleButton, SIGNAL(clicked()), this, SLOT(close()));

    imputLineEdit->setFocus();//
}

CloseCheck::~CloseCheck()
{

}

void CloseCheck::isOK()
{
    accept();
    return;
    //2015-08-07 delete password ensure quit
	QString a = randomLineEdit->text();
	QString b = imputLineEdit->text();
    //trimmed()
    if(randomLineEdit->text()==imputLineEdit->text())
    {
        accept();
    }
    else{
        QMessageBox::warning(this,tr("warning"),tr("ensure the imput to identify random_text!"),QMessageBox::Ok);
        randomLineEdit->clear();
		this->setRandomText();
        imputLineEdit->clear();
        imputLineEdit->setFocus();
    }
}

void CloseCheck::setRandomText()
{
	randomLineEdit->setText(QString("%1%2%3%4%5%6")
		.arg(qrand()%9)
		.arg(qrand()%9)
		.arg(qrand()%9)
		.arg(qrand()%9)
		.arg(qrand()%9)
		.arg(qrand()%9));
}

//#include "moc_CloseCheck.cpp"