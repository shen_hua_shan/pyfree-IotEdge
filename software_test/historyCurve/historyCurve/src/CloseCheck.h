/***********************************************************************
  *Copyright (c) 2011 Easy Communication Electtrical Power Tech.Co.Ltd
  *
  *File Name       : CloseCheck.h
  *File Mark       :
  *Summary         : App close check dialog
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#ifndef CLOSECHECK_H
#define CLOSECHECK_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QGridLayout;
class QFormLayout;
class QPushButton;
class QLineEdit;
class QLabel;
QT_END_NAMESPACE

class CloseCheck : public QDialog
{
	Q_OBJECT
public:
    explicit CloseCheck(QWidget * parent = 0, Qt::WindowFlags f = 0 );
    virtual ~CloseCheck();
private:
	void setRandomText();
signals:

public slots:
    void isOK();
private:
    QGridLayout *mQGridLayout;
    QFormLayout *mQFormLayout;
    QPushButton *okButton;
    QPushButton *canleButton;
    QLineEdit *randomLineEdit;
    QLineEdit *imputLineEdit;
    QLabel *userLabel;
    QLabel *secretLabel;
    QLabel *welcomeLabel;
};

#endif // CLOSECHECK_H