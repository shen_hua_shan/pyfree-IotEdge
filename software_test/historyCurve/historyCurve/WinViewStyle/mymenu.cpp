#include "mymenu.h"

MyMenu::MyMenu(QWidget * parent)
    : QMenu(parent)
{
    init();
}

MyMenu::MyMenu(const QString & title, QWidget * parent)
    : QMenu(title,parent)
{
    init();
}

MyMenu::~MyMenu()
{

}

void MyMenu::init()
{
    QFont _font("Courier New", 10);
    setFont(_font);
}
