#QT       += qwt
QT      += svg
QT      += opengl

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += printsupport
    QT += concurrent

}

unix{
    DEFINES += QWT_NO_SVG \

}

INCLUDEPATH              += $$ProDir/qwt
INCLUDEPATH              += $$ProDir/plot

HEADERS  +=  plot/plotWidget.h \
    plot/plotData.h \
    plot/plotcurve.h \
    plot/cuvre.h \
    plot/plotzoompicker.h \
    plot/plotlegend.h \
    plot/plothistogram.h \
    plot/histogram.h \
    plot/dataitem.h \
    plot/pfunc_qt.h \

SOURCES +=  plot/plotWidget.cpp \
    plot/plotData.cpp \
    plot/plotcurve.cpp \
    plot/cuvre.cpp \
    plot/plotzoompicker.cpp \
    plot/plotlegend.cpp \
    plot/plothistogram.cpp \
    plot/histogram.cpp \
    plot/dataitem.cpp \
    plot/pfunc_qt.cpp \

HEADERS  +=  $$ProDir/qwt/qwt_samples.h

win32{
    CONFIG(debug, debug|release) {
        LIBS       += $$ProDir\\lib\\debug\\qwtd.lib
    } else {
        LIBS       += $$ProDir\\lib\\release\\qwt.lib
    }
}

unix{
    LIBS             += $$ProDir/lib/linux/libqwt.a
}
