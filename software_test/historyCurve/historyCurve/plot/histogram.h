#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QVector>
#include <QString>
#include <QBrush>
#include <QColor>
#include <QPen>

#include "qwt/qwt_plot_histogram.h"
#include "qwt/qwt_symbol.h"
#include "plotData.h"

class MyQwtPlotHistogram;
//////////////////////////////////////////////////////////////////////////
class MyHistogram
{
public:
    MyHistogram(QString title,QwtPlotHistogram::HistogramStyle style);
    ~MyHistogram();

    QString getTitle() ;
    void attach( QwtPlot *plot );
    void detach();
    void addData(QwtIntervalSample value, QColor color);

    void setSamples();
//    void setSamples();
//    void setStyle(QwtPlotHistogram::HistogramStyle style);
//    void setPen(QColor color);
//    void setSymbolStyle(QwtSymbol::Style style);
private:
    QVector< QwtIntervalSample > val;
    QVector< QColor > m_Color;
    MyQwtPlotHistogram *m_QwtPlotHistogram;
};

#endif //HISTOGRAM_H