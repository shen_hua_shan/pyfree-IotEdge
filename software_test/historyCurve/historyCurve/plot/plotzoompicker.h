#ifndef PLOT_PLOTZOOMPICKER_H
#define PLOT_PLOTZOOMPICKER_H

#include <QMouseEvent>
#include <QRectF>

#include "qwt/qwt_plot_zoomer.h"
#include "qwt/qwt_plot_canvas.h"

class MyQwtPlotZoomer : public QwtPlotZoomer
{
    Q_OBJECT
public:
    explicit MyQwtPlotZoomer( QwtPlotCanvas *_canvas, bool doReplot = true );
    explicit MyQwtPlotZoomer( int xAxis, int yAxis,QwtPlotCanvas *_canvas, bool doReplot = true );
    void widgetMousePressEvent( QMouseEvent *event );
    void widgetMouseReleaseEvent( QMouseEvent *event );
    void widgetMouseDoubleClickEvent( QMouseEvent *event );

    void clearZoomStack();
signals:
    void resetcanvas();
public slots:

private:
};

#endif //PLOT_PLOTZOOMPICKER_H
