#ifndef PLOT_PLOTLEGEND_H
#define PLOT_PLOTLEGEND_H

#include "qwt/qwt_legend.h"

class PlotLegend : public QwtLegend
{
	Q_OBJECT
public:
	explicit PlotLegend( QWidget *parent = NULL );
	~PlotLegend();

// 	virtual void renderLegend( QPainter *, 
//         const QRectF &, bool fillBackground ) const;
// protected:
//     virtual QWidget *createWidget( const QwtLegendData &data ) const;
//     virtual void updateWidget( QWidget *widget, const QwtLegendData &data );

//     virtual void layoutContents();
// public slots:
//     virtual void updateLegend( const QwtPlotItem *plotItem, 
//         const QList<QwtLegendData> &data );
};
#endif	//PLOT_PLOTLEGEND_H
