win32{
QT += axcontainer
}

DEFINES += QAXSERVER \

INCLUDEPATH              += $$ProDir/SA_view

HEADERS +=  SA_view/appcachedata.h \
            SA_view/FileQt.h \
            SA_view/importconf.h \
            SA_view/columShowConf.h \
            SA_view/treeitem.h \
            SA_view/treemodel.h \
            SA_view/tableview.h \
            SA_view/SAQueryConf.h \
            SA_view/SATable.h \
            SA_view/SAThread.h \
            SA_view/SAView.h \
            SA_view/SAFileTable.h \

SOURCES +=  SA_view/appcachedata.cpp \
            SA_view/FileQt.cpp \
            SA_view/importconf.cpp \
            SA_view/columShowConf.cpp \
            SA_view/treeitem.cpp \
            SA_view/treemodel.cpp \
            SA_view/tableview.cpp \
            SA_view/SAQueryConf.cpp \
            SA_view/SATable.cpp \
            SA_view/SAThread.cpp \
            SA_view/SAView.cpp \
            SA_view/SAFileTable.cpp \
