QT += qml quick
!no_desktop: QT += widgets

include(shared/shared.pri)
include(src/src.pri)
#include(ice_src/ice_src.pri)

OTHER_FILES += \
    main.qml \
    content/AndroidDelegate.qml \
    content/ListPage.qml \
    content/MyTextInput.qml \

RESOURCES += \
    resources.qrc

target.path =touch
INSTALLS += target

CONFIG(debug, debug|release) {
    CONFIG          += console
    DEFINES         += _DEBUG
    TARGET          = pyfree_client_d
}else{
    DEFINES         += NODEBUG
    DEFINES         += NCONSOLE
    TARGET          = pyfree_client
}

#android conf
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += mobility
MOBILITY =

win32{
    RC_FILE += touch.rc
}

android-g++ {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    DISTFILES += \
        android/AndroidManifest.xml
}
