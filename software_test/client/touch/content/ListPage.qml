/****************************************************************************
**
** Copyright (C) 2017
**
****************************************************************************/

import QtQuick 2.2
//import QtQuick.Controls 1.2
import QtQuick.Controls 1.4
//import "Help.js" as Help

ScrollView {
    id:psview
    objectName:"psview"
    horizontalScrollBarPolicy:Qt.ScrollBarAsNeeded
//    width: parent.width
//    height: parent.height

    flickableItem.interactive: true

    ListView {              //MVC-V
        id: plview
        objectName: "plview"
        anchors.fill: parent
        model:submodel      //MVC-M
        delegate:subdelegete//MVC-C

        ListModel{
            id:submodel
            objectName: "submodel"
//            ListElement{
//    //            id:"1231259"
//                title:"pl01"
//                name:"ply"
//                pType:"213"
//                dtime:"06-09 13:45:34.612"
//                val:"0.8"
//            }
        }

        Component{
            id:subdelegete
            Item{
                width: parent.width
                height: Qt.platform.os === "android"?192:96;
//                property alias text: pitemtext.text;
                property alias myVal: pidval.text;
                property alias myTime: piddtime.text;
                property var key_On : "开";
                property var key_Off : "关";
                //listen the val change
                onMyValChanged:
                {
                    if(key_On==myVal){
                        pidval.color = "green";
                    }else if(key_Off==myVal){
                        pidval.color = "red";
                    }else{
                        pidval.color = "blue";
                    }
//                    console.log("pidval.text changed!");
                }

//                property var locale: Qt.locale();
//                property var curdate;
////                property string timeString
//                property string dateFormatString: "MM-dd hh:mm"

//                onMyTimeChanged:
//                {
//                    curdate = new Date();
////                    timeString = curdate.toLocaleString(locale, dateFormatString);
//                    var curd = curdate.toLocaleString(locale, dateFormatString);
//                    var upd = myTime.substring(0,11);
//                    if(curd!=upd){
//                        piddtime.color = "red";
//                    }else{
//                        piddtime.color = "white";
//                    }
////                    console.log("curd:"+curd);
////                    console.log("upd:"+upd);
//                }

                Rectangle{
                    anchors.fill: parent
                    color: "#11ffffff"
                }
                Text {
                    id:pidtitle
                    text: model.title
//                    text: model.title+"\n"+model.dtime+"\n"+model.val
//                    text: model.title+" ["+model.name+"("+model.pType+")]\n"+model.dtime+","+model.val
                    width: parent.width
                    height: Qt.platform.os === "android"?96:48
                    color: "white"
                    anchors.top: parent.top
                    anchors.bottom: piddtime.top
                    font.pixelSize: Qt.platform.os === "android"?72:36
//                    anchors.verticalCenter: pidtitle.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 12
                }
                Text {
                    id:piddtime
                    text: model.dtime
//                    text: model.title+"\n"+model.dtime+"\n"+model.val
//                    text: model.title+" ["+model.name+"("+model.pType+")]\n"+model.dtime+","+model.val
                    //width: parent.width
                    width: Qt.platform.os === "android"?parent.width-240:parent.width-120
                    height: Qt.platform.os === "android"?96:48
                    color: "white"
                    anchors.top: pidtitle.bottom
                    anchors.bottom: parent.bottom
                    font.pixelSize: Qt.platform.os === "android"?48:24
//                    anchors.verticalCenter: piddtime.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 12
                    anchors.rightMargin:this.width
                }
                TextInput{
                    id:pidval;
                    visible:true;
//                    cursorVisible:false;
//                    anchors.fill: parent;
//                    width: parent.width
                    width:Qt.platform.os === "android"?240:120
                    height: Qt.platform.os === "android"?96:48
                    color: "red"
                    anchors.top: pidtitle.bottom
                    anchors.bottom: parent.bottom
                    font.pixelSize: Qt.platform.os === "android"?64:32
                    anchors.right:parent.right
                    anchors.rightMargin:parent.rightMargin
//                    anchors.verticalCenter: pidval.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.platform.os === "android"?parent.width-240:parent.width-120
//                    validator: RegExpValidator { regExp: /^\d+(\.\d{0,3})?$/ }
//                    inputMethodHints: Qt.ImhDigitsOnly
                    horizontalAlignment:TextInput.AlignHCenter
                    verticalAlignment:TextInput.AlignVCenter
                    focus: true
                    text:model.val
                }

                Rectangle{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 12
                    height: 2
                    color: "#424246"
                }
    //            Image {
    //                anchors.right: parent.right
    //                anchors.rightMargin: 20
    //                anchors.verticalCenter: parent.verticalCenter
    //                source: "../images/navigation_next_item.png"
    //            }

                MouseArea {
                    id: mouse;
                    anchors.fill: parent;
                    onDoubleClicked: toggle();

                }
                function toggle() {
                    if(1==model.pType||3==model.pType)
                    {
//                        console.log("switch state :", model.val);
                        var _val = 0;
                        if (model.val == key_On)
                        {
                            model.val = key_Off;
                        }
                        else
                        {
                            model.val = key_On;
                            _val = 1;
                        }
//                        console.log("switch title :", model.title);
//                        console.log("switch state :", model.val);
//                        console.log("switch devid :", model.devid);
//                        console.log("switch id :", model.id);
                        appview.setPValue(model.devid,model.id,_val);
                    }else{
//                        pidval.cursorVisible = true;
//                        pidval.readOnly = false;
                        appview.setYCValue(model.devid,model.id,model.val);
                        console.log("toggle() and  model.pType=2/4" );
                    }
                }
            }

        }
    }
    property var pCount : 0;
    property var pObj : {"id":0,"name":""};
    property var pval_ : "off";
    property var pObjs : [] //ListModel.get函数造成的内存泄漏,采用自定义数组规避get函数调用
    property var pval_On : "开";
    property var pval_Off : "关";
    property var existf : 0;
    property var index_ : 0;
    function addPDef(devID,pID,name,desc, pType,val)
    {
//        console.log("ListPage addPDef");
//        console.log("pID :", pID);
//        console.log("name :", name);
//        console.log("desc :", desc);
//        console.log("pType :", pType);
//        console.log("val :", val);
        existf = 0;
        pCount = pObjs.length;
        for (index_ = 0; index_ < pCount; index_++)
        {
            pObj = pObjs[index_];
            if(pObj.id===pID)
            {
                existf = 1;
                break;
            }
        }
        if(0==existf){
            pval_ = getVal(pType,val);
            pObj = {
                "id":pID,
                "pType":pType
            };
            pObjs.push(pObj);
            pObj = {
                "id":pID,
                "pType":pType,
                "name":name,
                "title":desc,
                "dtime":"",
                "val":pval_,
                "devid":devID
            };
            submodel.append(pObj);
        }
    }

    function pValue(pID,dtime,val)
    {
        pCount = pObjs.length;
        for (index_ = 0; index_ < pCount; index_++)
        {
            pObj = pObjs[index_];
            if(pObj.id===pID)
            {
                pval_ = getVal(pObj.pType,val);
                submodel.setProperty(index_,"dtime",dtime);
                submodel.setProperty(index_,"val",pval_);
                break;
            }
        }
    }
    property var valStr : pval_Off;
    function getVal(pType,val)
    {
        valStr = val;
        if(1==pType||3==pType)
        {
            valStr = val>0?pval_On:pval_Off;
        }
        return valStr;
    }

//    style: ScrollViewStyle {
//        transientScrollBars: true
//        handle: Item {
//            implicitWidth: 14
//            implicitHeight: 26
//            Rectangle {
//                color: "#424246"
//                anchors.fill: parent
//                anchors.topMargin: 6
//                anchors.leftMargin: 4
//                anchors.rightMargin: 4
//                anchors.bottomMargin: 6
//            }
//        }
//        scrollBarBackground: Item {
//            implicitWidth: 14
//            implicitHeight: 26
//        }
//    }
}

