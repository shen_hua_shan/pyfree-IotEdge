/****************************************************************************
**
** Copyright (C) 2017
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.2
import "content"
//import "content/Help.js" as Help

ApplicationWindow {
    visible: true
    //
    width: Qt.platform.os === "android"?Screen.width:400
    height: Qt.platform.os === "android"?Screen.width:640
    id: appview
    objectName: "appview"

    Rectangle {
        objectName: "brect"
        color: "#212126"
        anchors.fill: parent
    }

    toolBar: BorderImage {
        id:headerBar
        border.bottom: 8
        source: "images/toolbar.png"
        width: parent.width
        height: Qt.platform.os === "android"?200:100
        objectName: "bimage"

        Rectangle {
            id: backButton
            objectName: "backButton"
            width: opacity ? (parent.height-40) : 0
            anchors.left: parent.left
            anchors.leftMargin: 10
            opacity: stackView.depth > 1 ? 1 : 0
            anchors.verticalCenter: parent.verticalCenter
            antialiasing: true
            height: parent.height-20
            radius: 4
            color: backmouse.pressed ? "#222" : "transparent"
            Behavior on opacity { NumberAnimation{} }
            Image {
                width: systitle.font.pixelSize
                height: systitle.font.pixelSize
                anchors.verticalCenter: parent.verticalCenter
                source: "images/navigation_previous_item.png"
            }
            MouseArea {
                id: backmouse
                objectName: "backmouse"
                anchors.fill: parent
                anchors.margins: -10
                onClicked:{
                    stackView.pop();
                    systitle.text = "client-test";
                }
            }
        }

        Text {
            id:systitle
            font.pixelSize: Qt.platform.os === "android"?108:48
            font.bold: true
            Behavior on x { NumberAnimation{ easing.type: Easing.OutCubic} }
            x: backButton.x + backButton.width + 20
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: "white"
            text: "client-test"
        }
        Rectangle {
            id: aboutconf
            objectName: "aboutconf"
            anchors.top: parent.top
            anchors.topMargin: 4
            anchors.right: parent.right
            anchors.rightMargin: 4
            width: Qt.platform.os === "android"?72:36
            height: Qt.platform.os === "android"?72:36
            color: "transparent"
            Image {
                anchors.fill: aboutconf
    //            anchors.verticalCenter: parent.verticalCenter
                source: "../images/pyfree.png"
            }
            MouseArea {
                id: aboutmouse
                objectName: "aboutmouse"
                anchors.fill: parent
                hoverEnabled: true
                onEntered:{
//                    console.log("aboutmouse onEntered");
                    productread.visible = true;
                }

                onExited:{
//                    console.log("aboutmouse onExited");
                    productread.visible = false;
                }
//                onClicked:{
//                    console.log("aboutmouse onClicked");
//                }
            }
        }
        Rectangle {
            id: ipconf
            objectName: "ipconf"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 4
            anchors.right: parent.right
            anchors.rightMargin: 4
            width: Qt.platform.os === "android"?72:36
            height: Qt.platform.os === "android"?72:36
            opacity: 1
            radius: 2
            color: "transparent"
            Image {
                anchors.fill: ipconf
                source: "../images/conf.png"
            }
            MouseArea {
                id: confmouse
                objectName: "confmouse"
                anchors.fill: parent
                onClicked:{
//                    console.log("confmouse onClicked");
                    appview.setIPStr();
                }
            }
        }
    }
    ListModel {  
        id: pageModel
        objectName: "pageModel"
    }

    StackView {
        id: stackView
        objectName: "stackView"
        anchors.fill: parent
        // Implements back key navigation
        focus: true
        Keys.onReleased: if (event.key === Qt.Key_Back && stackView.depth > 1) {
                             stackView.pop();
                             systitle.text = "client-test";
                             event.accepted = true;
                         }else if (event.key === Qt.Key_Home /*&& stackView.depth <=1*/) {
                             event.accepted = true;
                             Qt.quit();
                         }else{
                            event.accepted = true;
                         }

        initialItem: Item {
            objectName: "mitem"
            width: parent.width
            height: parent.height
            ListView {
                id:lview
                objectName: "lview"
                model: pageModel
                delegate: ldelegate
                anchors.fill: parent

                Component{
                    id:ldelegate
                    AndroidDelegate {
//                        text: title+"\n["+name+"("+dtype+")]"
                        text: title
//                        onClicked: stackView.push(Qt.resolvedUrl(page))
                        onClicked: {
                            stackView.push(page);
                            for (var i = 0; i < pageModel.count; i++)
                            {
                                if(pageModel.get(i).page===page)
                                {
                                   systitle.text = pageModel.get(i).title;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle{
        id:productread
        anchors.bottom: parent.bottom
        width:parent.width
        height:Qt.platform.os === "android"?280:128
        visible:false
        Text {
            id:abouttext
            font.pixelSize: Qt.platform.os === "android"?24:12
            font.bold: true
            anchors.fill: parent
            anchors.topMargin: 5
            anchors.leftMargin: 10
//            color: "white"
            text: "产品：client-test\n"
                  +"文件: pyfree_client\n"
                  +"版本：v1.0\n"
                  +"版权：Copyright©2020-2021\n"
                  +"邮箱：py8105@163.com"
        }
    }

    MyTextInput {
        id: ycinput
        property var devID: -1
        property var pID: -1
        property var ipf: 0
        text: "0.0"
        height:Qt.platform.os === "android"?96:48
        width: parent.width
//        font.pixelSize: 28
//        anchors.top: stackView.bottom
        anchors.bottom: parent.bottom
        inputMethodHints: Qt.ImhDigitsOnly
        visible:false
        onAccepted:
        {
            if(devID>0&&pID>0){
//                console.log("MyTextInput.onAccepted devID=",devID," pID=",pID," val=",text);
                appview.setPValue(devID,pID,ycinput.text);
                devID = -1;
                pID = -1;
            }
            if(ipf>0){
//                console.log("ycinput.text=",ycinput.text);
                appview.onIpConf(ycinput.text);
                ipf = 0;
            }

            ycinput.visible = false;
            ycinput.focus = false;
            stackView.focus = true;
        }
    }

    function setYCValue(devID,pID,oldval)
    {
//        console.log("devID :", devID," pID :", pID," val :", oldval);
        ycinput.focus = true;
        stackView.focus = false;
        ycinput.visible = true;
        ycinput.text = oldval;
        ycinput.devID = devID;
        ycinput.pID = pID;
//        console.log("Qt.platform.os :", Qt.platform.os);
    }

    property var cacheIP: "127.0.0.1"

    function setIPStr()
    {
        ycinput.focus = true;
        stackView.focus = false;
        ycinput.visible = true;
        ycinput.text = cacheIP;
        ycinput.ipf = 1;
//        console.log("cacheIP=",cacheIP,"ycinput.ipf=",ycinput.ipf);
    }
    //
    signal setPValue(int devID,int pID, real val);
    signal onIpConf(string ip);

    function ipCache(ip_)
    {
//        console.log("ip_ :", ip_);
        cacheIP = ip_;
    }

    property var devModel : Qt.createComponent("content/ListPage.qml");
    property var devCount : 0;
    property var devObj : { "id": 0, "name": "" };
    property var devObjs: []//ListModel.get函数造成的内存泄漏,采用自定义数组尽量规避get函数调用
    property var existf : 0;
    property var index_dev : 0;
    function addDev(devid,devtype,name,desc)
    {
//        console.log("devID :", devid," devType :", devtype," name :", name," desc :", desc);
        existf = 0;
        devCount = devObjs.length;
        for (index_dev = 0; index_dev < devCount; index_dev++)
        {
            devObj = devObjs[index_dev];
            if(devObj.id===devid)
            {
//                console.log("dev:", desc,"is exist");
                existf = 1;
                break;
            }
        }
        if(0==existf){            
//            console.log("devObjs size=:", devObjs.length);
            devObj = {
                "id":devid,
                "dtype":devtype,
                "name":name,
                "title":desc,
                "page":devModel.createObject(devid)
            };
            pageModel.append(devObj);
            devObjs.push(pageModel.get(devObjs.length));
        }
    }

    function addPInfo(devID,pID,name,desc, pType,val)
    {
//        console.log("devID :", devID," pID :", pID," name :", name," desc :", desc," pType :", pType," val :", val);
        devCount = devObjs.length;
        for (index_dev = 0; index_dev < devCount; index_dev++)
        {
            devObj = devObjs[index_dev];
            if(devObj.id===devID)
            {
//                devObj = pageModel.get(index_dev);//暂时无法更好的办法规避
                devObj.page.addPDef(devID,pID,name,desc, pType,val);
                break;
            }
        }
    }

    function pValue(devID,pID,dtime,val)
    {
//        console.log("devID :", devID," pID :", pID," dtime :", dtime," val :", val);
        devCount = devObjs.length;
        for (index_dev = 0; index_dev < devCount; index_dev++)
        {
            devObj = devObjs[index_dev];
            if(devObj.id===devID)
            {
//                devObj = pageModel.get(index_dev);//暂时无法更好的办法规避
                devObj.page.pValue(pID,dtime,val);
                break;
            }
        }
    }
}
