多媒体播放器状态信息点
[3,YX]1，播放；0结束
[4,YX]1，播放；0暂停
[5,YX]1，全屏；0，退出全屏

[101，YC]播放方式
** 1，当前媒体播放一次；
** 2，当前媒体播放循环；
** 3，列表顺序播放一次（默认）
** 4，列表循环播放
** 5，列表随机播放
[102,YC]列表媒体数量
[103,YC]当前媒体索引
[104,YC]播放位置
[105,YC]声音大小{0~100}
[106,YC]媒体状态{1~9}
[107,YC]播放速率


[301,YK]下一媒体
[302，YK]前一媒体
#[303,YK]快进10秒
#[304，YK]快退10秒

[媒体解析库支持]
qt本身不做流媒体编解码,引起其需要第三方媒体解析库的支持
一般win/LAVFilters,linux/ffmpeg,Android/ffmpeg，运行前需要先安装支持的媒体库

[Qt翻译问题]
本demo并没有做翻译处理
1)qt的命令工具进入当前目录
2)lupdate player.pro生成源语言文件(.ts)
3)启动翻译的UI工具(linguits.exe，非qtcreate所在目录,qt源的bin目录)进行可视化翻译
4)lrelease player.pro生成翻译文件(.qm)

                [Qt5开发Android：设置图标与强制横屏]
编译成功后，会有一个Android-build 的目录，这个目录里有一个AndroidManifest.xml文件；
在工程目录里建一个android 的文件夹，将 AndroidManifest.xml放入android 文件夹中；

在 pro 里加入下面这句话：
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

然后修改  AndroidManifest.xml 文件：
    android:screenOrientation="landscape"

Activity在屏幕当中显示的方向。属性值可以是下表中列出的一个值：
"unspecified"       默认值，由系统来选择方向。它的使用策略，以及由于选择时特定的上下文环境，可能会因为设备的差异而不同。
"user"              使用用户当前首选的方向。
"behind"            使用Activity堆栈中与该Activity之下的那个Activity的相同的方向。
"landscape"         横向显示（宽度比高度要大）
"portrait"          纵向显示（高度比宽度要大）
"reverseLandscape"  与正常的横向方向相反显示，在API Level 9中被引入。
"reversePortrait"   与正常的纵向方向相反显示，在API Level 9中被引入。
"sensorLandscape"   横向显示，但是基于设备传感器，既可以是按正常方向显示，也可以反向显示，在API Level 9中被引入。
"sensorPortrait"    纵向显示，但是基于设备传感器，既可以是按正常方向显示，也可以反向显示，在API Level 9中被引入。
"sensor"            显示的方向是由设备的方向传感器来决定的。显示方向依赖与用户怎样持有设备；当用户旋转设备时，显示的方向会改变。但是，默认情况下，有些设备不会在所有的四个方向上都旋转，因此要允许在所有的四个方向上都能旋转，就要使用fullSensor属性值。
"fullSensor"        显示的方向（4个方向）是由设备的方向传感器来决定的，除了它允许屏幕有4个显示方向之外，其他与设置为“sensor”时情况类似，不管什么样的设备，通常都会这么做。例如，某些设备通常不使用纵向倒转或横向反转，但是使用这个设置，还是会发生这样的反转。这个值在API Level 9中引入。
"nosensor"          屏幕的显示方向不会参照物理方向传感器。传感器会被忽略，所以显示不会因用户移动设备而旋转。除了这个差别之外，系统会使用与“unspecified”设置相同的策略来旋转屏幕的方向。

可在QT中打开AndroidManifest.xml 文件，直接修改程序的icon
