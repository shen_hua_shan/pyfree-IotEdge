QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
#win and linux conf
#CONFIG += qt warn_on debug_and_release thread static

QT += network \
      xml \
      multimedia \
      multimediawidgets \
      widgets

HEADERS = \
    player.h \
    playercontrols.h \
    playlistmodel.h \
    videowidget.h \
    histogramwidget.h \
    socketServer.h \
    pfunc_qt.h \
    routing.h \
    confdeal.h

SOURCES = main.cpp \
    player.cpp \
    playercontrols.cpp \
    playlistmodel.cpp \
    videowidget.cpp \
    histogramwidget.cpp \
    socketServer.cpp \
    pfunc_qt.cpp \
    routing.cpp \
    confdeal.cpp

target.path = player
INSTALLS += target

DISTFILES += \
    readme.txt
#android conf
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += mobility
MOBILITY =

RESOURCES += \
    player.qrc

#ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
win32{
    TARGET = player
    RC_FILE += player.rc
}

android-g++ {
    TARGET = player_android
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    DISTFILES += \
        android/AndroidManifest.xml
}

#unix{
#   TARGET = player_linux
#   LIBS += -L"/usr/lib64" -L"/usr/lib" -lpulse-mainloop-glib -lpulse -lglib-2.0
   #INCLUDEPATH +="/opt/ffmpeg/include"

   #LIBS += -L"/opt/ffmpeg/lib" -lavutil -lavformat -lavcodec -lavdevice -lavfilter -lswresample -lswscale
#}

#翻译
TRANSLATIONS += languages/player_cn.ts
