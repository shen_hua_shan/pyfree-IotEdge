// MonitorService.cpp : 定义控制台应用程序的入口点。
//

#ifdef WIN32
#pragma warning(disable: 4995)  //for swprintf

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>

#include "atlcomtime.h"
#pragma comment(lib, "advapi32.lib")

#include "Log.h"

int MyMain(int argc, char * argv[]);		//Actual Main function, should be defined
extern char SVCNAME[128];					//SVC Name, should be defined
extern char SVCDESC[256];					//SVC Desc, should be defined
void SvcQuery(char* svr, int &svc_state);	//根据服务名查询服务状态
void SvcStart(char *svr);					//根据服务名启动服务
void SvcStop(char *svr);					//根据服务名停止服务
void SvcRestart(char *svr);					//根据服务名重启服务

namespace WINSVC
{
SERVICE_STATUS          gSvcStatus; 
SERVICE_STATUS_HANDLE   gSvcStatusHandle; 
HANDLE                  ghSvcStopEvent = NULL;

/**
 * 本服务安装
 * @param {void}
 * @retuan {void} 
 */
VOID SvcInstall(void);
/**
 * 本服务卸载
 * @param {void}
 * @retuan {void} 
 */
VOID DoDeleteSvc(void);
/**
 * 查询服务
 * @param svr {char*} 服务名
 * @param svc_state {int&} 服务状态
 * @retuan {void} 
 */
VOID SvcQuery(char* svr, int &svc_state);
/**
 * 启动服务
 * @param svr {char*} 服务名
 * @retuan {void} 
 */
VOID SvcStart(char *svr);
/**
 * 停止服务
 * @param svr {char*} 服务名
 * @retuan {void} 
 */
VOID SvcStop(char *svr);
/**
 * 重启服务
 * @param svr {char*} 服务名
 * @retuan {void} 
 */
VOID SvcRestart(char *svr);

VOID WINAPI SvcCtrlHandler( DWORD ); 
VOID WINAPI SvcMain( DWORD, LPTSTR * ); 

VOID ReportSvcStatus( DWORD, DWORD, DWORD );
/**
 * 本服务初始化
 * @param svr {char*} 服务名
 * @retuan {void} 
 */
VOID SvcInit( DWORD, LPTSTR * ); 
VOID SvcReportEvent( LPTSTR );
//BOOL ShowMenu(void);
VOID CALLBACK MainThread(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime);

HANDLE m_HThread;
DWORD m_dwThreadID;
BOOL g_bRun = FALSE;
}

void __cdecl _tmain(int argc, TCHAR *argv[]) 
{ 
	if( lstrcmpi( argv[1], TEXT("install")) == 0 )
	{
		WINSVC::SvcInstall();
		return;
	}
	else if( lstrcmpi( argv[1], TEXT("uninstall")) == 0 )
	{
		WINSVC::DoDeleteSvc();
		return;
	}
	SERVICE_TABLE_ENTRY DispatchTable[] = 
	{ 
		{ SVCNAME, (LPSERVICE_MAIN_FUNCTION) WINSVC::SvcMain }, 
		{ NULL, NULL } 
	}; 

	WINSVC::g_bRun = TRUE;
	if (!StartServiceCtrlDispatcher( DispatchTable )) 
	{ 
		WINSVC::SvcReportEvent(TEXT("StartServiceCtrlDispatcher"));
		MyMain(argc, argv);
		MSG msg;
		while (GetMessage(&msg, 0, 0, 0))
			DispatchMessage(&msg);
	} 
};

void SvcQuery(char* svr, int &svc_state)
{
	WINSVC::SvcQuery(svr, svc_state);
};

void SvcStart(char *svr)
{
	WINSVC::SvcStart(svr);
};

void SvcStop(char *svr)
{
	WINSVC::SvcStop(svr);
};

void SvcRestart(char *svr)
{
	WINSVC::SvcRestart(svr);
};

VOID WINSVC::SvcInstall()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	TCHAR szPath[MAX_PATH];

	if( !GetModuleFileName( NULL, szPath, MAX_PATH ) )
	{
		CLogger::createInstance()->Log(MsgError,"Install service fail (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Get a handle to the SCM database. 

	schSCManager = OpenSCManager( 
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager) 
	{
		CLogger::createInstance()->Log(MsgError,"Failed to open service manager (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Create the service

	schService = CreateService( 
		schSCManager,              // SCM database 
		SVCNAME,                   // name of service 
		SVCNAME,                   // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		szPath,                    // path to service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 

	if (schService == NULL) 
	{
		CLogger::createInstance()->Log(MsgError,"Failed to create service (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
	else{
		printf("%s\n",SVCDESC);//log输出不及时造成无显示
		CLogger::createInstance()->Log(MsgInfo,SVCDESC); 
	}
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager);
}


//
// Purpose: 
//   Deletes a service from the SCM database
//
// Parameters:
//   None
// 
// Return value:
//   None
//
VOID WINSVC::DoDeleteSvc()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	//SERVICE_STATUS ssStatus; 

	// Get a handle to the SCM database. 

	schSCManager = OpenSCManager( 
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager) 
	{
		CLogger::createInstance()->Log(MsgError,"Failed to open service manager (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Get a handle to the service.

	schService = OpenService( 
		schSCManager,       // SCM database 
		SVCNAME,          // name of service 
		DELETE);            // need delete access 

	if (schService == NULL)
	{ 
		CLogger::createInstance()->Log(MsgError,"Failed to get service (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Delete the service.

	if (! DeleteService(schService) ) 
	{
		CLogger::createInstance()->Log(MsgError,"Failed to delete service (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
	}
	else{ 
		printf("Service successfully removed !\n");//log输出不及时造成无显示
		CLogger::createInstance()->Log(MsgInfo,"Service successfully removed !"); 
	}
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager);
}

VOID WINSVC::SvcQuery(char *svr, int &svc_state)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	// Get a handle to the SCM database. 
	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to open service manager (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}
	// Get a handle to the service.
	schService = OpenService(
		schSCManager,       // SCM database 
		svr,				// name of service 
		SERVICE_QUERY_STATUS);     // need query access 

	if (schService == NULL)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to get service(%s) and error(%d), %s %s %d!"
			, svr, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
	//调用QueryServiceStatus函数
	SERVICE_STATUS sStatus = { 0 };
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	switch (sStatus.dwCurrentState)
	{
	case SERVICE_STOP_PENDING: case SERVICE_STOPPED:
		svc_state = 1;
		break;
	case SERVICE_START_PENDING: case SERVICE_RUNNING: case SERVICE_CONTINUE_PENDING:
		svc_state = 2;
		break;
	case SERVICE_PAUSE_PENDING: case SERVICE_PAUSED:
		svc_state = 3;
		break;
	default:
		svc_state = 0;
		break;
	}
	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
};

VOID WINSVC::SvcStart(char *svr)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	// Get a handle to the SCM database. 
	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to open service manager (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}
	// Get a handle to the service.
	schService = OpenService(
		schSCManager,       // SCM database 
		svr,				// name of service 
		SERVICE_START | SERVICE_QUERY_STATUS);     // need start and query access 

	if (schService == NULL)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to get service(%s) and error(%d), %s %s %d!"
			, svr,GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
	StartService(schService, 0, NULL);//开始Service
	//调用QueryServiceStatus函数
	SERVICE_STATUS sStatus = { 0 };
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_RUNNING == sStatus.dwCurrentState || SERVICE_START_PENDING == sStatus.dwCurrentState)
	{
		CLogger::createInstance()->Log(MsgInfo, "start service(%s) success, %s %s %d!"
			, svr, __FILE__, __FUNCTION__, __LINE__);
	}
	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
};

VOID WINSVC::SvcStop(char *svr)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	// Get a handle to the SCM database. 
	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to open service manager(%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}
	// Get a handle to the service.
	schService = OpenService(
		schSCManager,       // SCM database 
		svr,				// name of service 
		SERVICE_STOP | SERVICE_QUERY_STATUS);            // need stop or query access 

	if (schService == NULL)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to get service(%s) and error(%d), %s %s %d!"
			, svr,GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
	//调用QueryServiceStatus函数
	SERVICE_STATUS sStatus = { 0 };
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_RUNNING == sStatus.dwCurrentState || SERVICE_PAUSED == sStatus.dwCurrentState)
	{
		ControlService(schService, SERVICE_CONTROL_STOP, &sStatus);
	}
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_STOPPED == sStatus.dwCurrentState || SERVICE_STOP_PENDING == sStatus.dwCurrentState)
	{
		CLogger::createInstance()->Log(MsgInfo, "stop service(%s) success, %s %s %d!"
			, svr, __FILE__, __FUNCTION__, __LINE__);
	}
	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
};

VOID WINSVC::SvcRestart(char *svr)
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	// Get a handle to the SCM database. 
	schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to open service manager and error(%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}
	// Get a handle to the service.
	schService = OpenService(
		schSCManager,       // SCM database 
		svr,				// name of service 
		SERVICE_START | SERVICE_STOP | SERVICE_QUERY_STATUS);     // need pause and query access 

	if (schService == NULL)
	{
		CLogger::createInstance()->Log(MsgError, "Failed to get service(%s) and error(%d), %s %s %d!"
			, svr,GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
	//调用QueryServiceStatus函数
	SERVICE_STATUS sStatus = { 0 };
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_RUNNING == sStatus.dwCurrentState)
	{
		ControlService(schService, SERVICE_CONTROL_STOP, &sStatus);
	}
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_STOPPED == sStatus.dwCurrentState || SERVICE_STOP_PENDING == sStatus.dwCurrentState)
	{
		CLogger::createInstance()->Log(MsgInfo, "stop service (%s) success, %s %s %d!"
			, svr, __FILE__, __FUNCTION__, __LINE__);
		StartService(schService, 0, NULL);//开始Service
	}
	if (!QueryServiceStatus(schService, &sStatus))
	{
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return;
	}
	if (SERVICE_RUNNING == sStatus.dwCurrentState || SERVICE_START_PENDING == sStatus.dwCurrentState)
	{
		CLogger::createInstance()->Log(MsgInfo, "restart service (%s) success, %s %s %d!"
			, svr, __FILE__, __FUNCTION__, __LINE__);
	}
	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);
};
//
// Purpose: 
//   Entry point for the service
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
// 
// Return value:
//   None.
//
VOID WINAPI WINSVC::SvcMain( DWORD dwArgc, LPTSTR *lpszArgv )
{
	// Register the handler function for the service

	gSvcStatusHandle = RegisterServiceCtrlHandler( 
		SVCNAME, 
		SvcCtrlHandler);

	if( !gSvcStatusHandle )
	{ 
		SvcReportEvent(TEXT("RegisterServiceCtrlHandler")); 
		return; 
	} 

	// These SERVICE_STATUS members remain as set here

	gSvcStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS; 
	gSvcStatus.dwServiceSpecificExitCode = 0;    

	// Report initial status to the SCM

	ReportSvcStatus( SERVICE_START_PENDING, NO_ERROR, 3000 );

	// Perform service-specific initialization and work.

	SvcInit( dwArgc, lpszArgv );
}

//
// Purpose: 
//   The service code
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
// 
// Return value:
//   None
//
VOID WINSVC::SvcInit( DWORD dwArgc, LPTSTR *lpszArgv)
{
	// TO_DO: Declare and set any required variables.
	//   Be sure to periodically call ReportSvcStatus() with 
	//   SERVICE_START_PENDING. If initialization fails, call
	//   ReportSvcStatus with SERVICE_STOPPED.

	// Create an event. The control handler function, SvcCtrlHandler,
	// signals this event when it receives the stop control code.

	TCHAR szPath[MAX_PATH];
	GetModuleFileName( NULL, szPath, MAX_PATH );
	TCHAR drive[MAX_PATH],dir[MAX_PATH],fname[MAX_PATH],ext[MAX_PATH];
	_tsplitpath_s( szPath,drive,dir,fname,ext );
	strcpy_s( szPath, drive );
	strcat_s( szPath, dir );
	SetCurrentDirectory( szPath );

	ghSvcStopEvent = CreateEvent(
		NULL,    // default security attributes
		TRUE,    // manual reset event
		FALSE,   // not signaled
		NULL);   // no name

	if ( ghSvcStopEvent == NULL)
	{
		ReportSvcStatus( SERVICE_STOPPED, NO_ERROR, 0 );
		return;
	}
	//{
	//	int nDirSend  = 0;
	//	if(lpszArgv[1])
	//	{
	//		nDirSend = atoi(lpszArgv[1]);
	//		if(nDirSend > 2)
	//			nDirSend = 2;
	//	}
	//	CControlCenter::createInstance()->SetDir(nDirSend);
	//}	
	m_HThread = CreateThread( (LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)MainThread,	0, 0,   &m_dwThreadID);
	if( m_HThread != NULL )
	{
		g_bRun = TRUE;
	}
	// Report running status when initialization is complete.
	ReportSvcStatus( SERVICE_RUNNING, NO_ERROR, 0 );
	bool bWriteFlag = false;
	while(1)
	{
		// Check whether to stop the service.

		WaitForSingleObject(ghSvcStopEvent, INFINITE);
		if(!bWriteFlag)
		{
			CLogger::createInstance()->Log(MsgError,"Service normal stop: %s %s %d"
			, __FILE__, __FUNCTION__, __LINE__);
			bWriteFlag = true;
		}
		g_bRun = FALSE;
		ReportSvcStatus( SERVICE_STOPPED, NO_ERROR, 0 );
	}
}

//
// Purpose: 
//   Sets the current service status and reports it to the SCM.
//
// Parameters:
//   dwCurrentState - The current state (see SERVICE_STATUS)
//   dwWin32ExitCode - The system error code
//   dwWaitHint - Estimated time for pending operation, 
//     in milliseconds
// 
// Return value:
//   None
//
VOID WINSVC::ReportSvcStatus( DWORD dwCurrentState,
	DWORD dwWin32ExitCode,
	DWORD dwWaitHint)
{
	static DWORD dwCheckPoint = 1;

	// Fill in the SERVICE_STATUS structure.

	gSvcStatus.dwCurrentState = dwCurrentState;
	gSvcStatus.dwWin32ExitCode = dwWin32ExitCode;
	gSvcStatus.dwWaitHint = dwWaitHint;

	if (dwCurrentState == SERVICE_START_PENDING)
		gSvcStatus.dwControlsAccepted = 0;
	else gSvcStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

	if ( (dwCurrentState == SERVICE_RUNNING) ||
		(dwCurrentState == SERVICE_STOPPED) )
		gSvcStatus.dwCheckPoint = 0;
	else gSvcStatus.dwCheckPoint = dwCheckPoint++;

	// Report the status of the service to the SCM.
	SetServiceStatus( gSvcStatusHandle, &gSvcStatus );
}

//
// Purpose: 
//   Called by SCM whenever a control code is sent to the service
//   using the ControlService function.
//
// Parameters:
//   dwCtrl - control code
// 
// Return value:
//   None
//
VOID WINAPI WINSVC::SvcCtrlHandler( DWORD dwCtrl )
{
	// Handle the requested control code. 

	switch(dwCtrl) 
	{  
	case SERVICE_CONTROL_STOP: 
		ReportSvcStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

		// Signal the service to stop.

		SetEvent(ghSvcStopEvent);
		ReportSvcStatus(gSvcStatus.dwCurrentState, NO_ERROR, 0);

		return;

	case SERVICE_CONTROL_INTERROGATE: 
		break; 

	default: 
		break;
	} 

}

//
// Purpose: 
//   Logs messages to the event log
//
// Parameters:
//   szFunction - name of function that failed
// 
// Return value:
//   None
//
// Remarks:
//   The service must have an entry in the Application event log.
//
VOID WINSVC::SvcReportEvent(LPTSTR szFunction) 
{ 
	HANDLE hEventSource;
	LPCTSTR lpszStrings[2];
	TCHAR Buffer[80];

	hEventSource = RegisterEventSource(NULL, SVCNAME);

	if( NULL != hEventSource )
	{
		StringCchPrintf(Buffer, 80, TEXT("%s failed with %d, %s %s %d"), szFunction
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);

		lpszStrings[0] = SVCNAME;
		lpszStrings[1] = Buffer;

		ReportEvent(hEventSource,        // event log handle
			EVENTLOG_ERROR_TYPE, // event type
			0,                   // event category
			0,           // event identifier
			NULL,                // no security identifier
			2,                   // size of lpszStrings array
			0,                   // no binary data
			lpszStrings,         // array of strings
			NULL);               // no binary data

		DeregisterEventSource(hEventSource);
	}
}
/*
BOOL WINSVC::ShowMenu(void)
{
	HDESK   hdeskCurrent;
	HDESK   hdesk;
	HWINSTA hwinstaCurrent;
	HWINSTA hwinsta;

	hwinstaCurrent = GetProcessWindowStation();
	if (hwinstaCurrent == NULL)
	{
		//LogEvent(_T("get window station err"));
		return FALSE;
	}

	hdeskCurrent = GetThreadDesktop(GetCurrentThreadId());
	if (hdeskCurrent == NULL)
	{
		//LogEvent(_T("get window desktop err"));
		return FALSE;
	}

	//打开winsta0
	hwinsta = OpenWindowStation(_T("winsta0"), FALSE,                          
		WINSTA_ACCESSCLIPBOARD   |
		WINSTA_ACCESSGLOBALATOMS |
		WINSTA_CREATEDESKTOP     |
		WINSTA_ENUMDESKTOPS      |
		WINSTA_ENUMERATE         |
		WINSTA_EXITWINDOWS       |
		WINSTA_READATTRIBUTES    |
		WINSTA_READSCREEN        |
		WINSTA_WRITEATTRIBUTES);
	if (hwinsta == NULL)
	{
		//LogEvent(_T("open window station err"));
		return FALSE;
	}

	if (!SetProcessWindowStation(hwinsta))
	{
		//LogEvent(_T("Set window station err"));
		return FALSE;
	}

	//打开desktop
	hdesk = OpenDesktop(_T("default"), 0, FALSE,                
		DESKTOP_CREATEMENU |
		DESKTOP_CREATEWINDOW |
		DESKTOP_ENUMERATE    |
		DESKTOP_HOOKCONTROL  |
		DESKTOP_JOURNALPLAYBACK |
		DESKTOP_JOURNALRECORD |
		DESKTOP_READOBJECTS |
		DESKTOP_SWITCHDESKTOP |
		DESKTOP_WRITEOBJECTS);
	if (hdesk == NULL)
	{
		//LogEvent(_T("Open desktop err"));

		return FALSE;
	}

	SetThreadDesktop(hdesk);

	//到这一步，我们获取了和用户交互（如显示窗口）的权利
	//m_TrayIcon.Create(NULL,	WM_ICON_NOTIFY,	_T("实时通道监听服务"), ::LoadIcon(NULL, IDI_ASTERISK), IDR_POPUP_MENU);
	//m_TrayIcon.SetMenuDefaultItem(2, TRUE);       

	if (!SetProcessWindowStation(hwinstaCurrent))
		return FALSE;

	if (!SetThreadDesktop(hdeskCurrent))
		return FALSE;

	if (!CloseWindowStation(hwinsta))
		return FALSE;

	if (!CloseDesktop(hdesk))
		return FALSE;

	return TRUE;
}
*/

VOID CALLBACK WINSVC::MainThread(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
{	
	int argc = 0;
	char * pArgv = NULL;
	MyMain(argc, &pArgv);
	CLogger::createInstance()->Log(MsgInfo,"Main thread quit successfully");
}
#endif
