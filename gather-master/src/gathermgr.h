#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _GATHER_MGR_H_
#define _GATHER_MGR_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : gathermgr.h
  *File Mark       : 
  *Summary         : 数据采集线程和数据转发线程的管理与业务中台，负责数据中转
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"
#include "queuedata_single.h"
#include "datadef.h"

class BusinessDef;
class DiskSpaceMgr;
class TranChannel;
class GatherChannel;

class GatherMgr : public acl::thread
{
public:
	GatherMgr();
	~GatherMgr();
	void* run();
	void initPort();
private:
	//运行参数及业务数据集
	BusinessDef *ptrBDef;				
	//写入采集端口的缓存数据集,由本线程负责业务中转
	QueueDataSingle<TransmitToGChannel> *ptr_ToChannelDatas;
	//写入转发端口的数据缓存,由本线程进行业务中转
	QueueDataSingle<ChannelToTransmit> *ptr_ToTranDatas;
	//日志自动化删除管理
	DiskSpaceMgr *m_DiskSpaceMgr;	
	//采集端口集
	std::map<int, GatherChannel*> gchannel_map;
	//转发端口集
	std::map<int, TranChannel*> tchannel_map;
};


#endif