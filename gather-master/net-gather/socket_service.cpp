#include "socket_service.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#endif
#include "socket_private_acl.h"
#include "Log.h"

SocketService::SocketService(void)
	: socket_acl(NULL)
{
}

SocketService::~SocketService(void)
{
}

void SocketService::setPDataPtr(SocketPrivate_ACL *socket_acl_)
{
	socket_acl=socket_acl_;
};

void* SocketService::run()
{
	if (NULL == socket_acl) 
	{
		CLogger::createInstance()->Log(MsgError,
			"SocketService start fail for socket_acl is NULL,[%s %s %d]!"
			, __FILE__, __FUNCTION__, __LINE__);
		return 0;
	}
	while(1)
	{
		socket_acl->Accept();
		usleep(10000);
	}
	return NULL;
}
