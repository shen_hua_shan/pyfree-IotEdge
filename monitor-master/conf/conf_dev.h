#ifndef _CONF_DEF_H_
#define _CONF_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_dev.h
  *File Mark       : 
  *Summary         : 设备数据业务模型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <vector>
#include <map>

#include "dtypedef.h"

namespace pyfree
{

//设备组属性
struct DevGroupProPerty 
{
	//设备组编号
	long long		devGID;
	std::string		name;
	std::string 	desc;
};

//设备属性
struct DevProPerty
{
	//设备编号 设备类型
	long long		devID;
	DevType			devType;
	//设备组编号
	long long		gID;
	std::string		name;
	std::string		desc;
	AliyunTriples   device_triples;	//网关三元组
	std::map<std::string, std::string> dev_tags;	//设备标签集,用于阿里云物联网
};

//点信息
struct PInfo
{
	//常量数据
	unsigned int pID;
	PType		pType;
	std::string name;
	std::string desc;
	std::string aliyun_key;
	//
	bool	changeRecord;	//是否记录
	//Func func;
	float	ratio;			//系数
	float	base;			//偏移
	int		clientOrder;	//是否客户端需求
	int		upLoopTime;		//数据上送间隔巡检时间
	int		upLoopTime_ali;	//阿里云数据上送间隔巡检时间
	//event arg
	float   valLimitUp;		//数值上限制
	float   valLimitDown;	//数值下限制
	float   comValLimit;	//越变约束基准值
	float   comRate;		//越变约束比例
	//
	int		evtStartTime;	//告警判定开始时间,单位分钟,默认是0,取值0~1440
	int		evtEndTime;		//告警判定结束时间,单位分钟,默认是0,取值0~1440
	//变量数据
	float				defVal;				//值
	unsigned long long	valTimeClock;		//记录变化值时的挂钟时间，毫秒
	unsigned long long	valUpdateClock;		//记录更新值时的挂钟时间，毫秒
	unsigned int		shakeTimeInterval;	//抖动时间间隔,默认1000毫秒
	bool				ValChange;			//是否变化
	unsigned int		markT;				//标记时间
	unsigned int		markT_ali;			//标记时间
	bool				ValChangeCopy;		//是否变化副本
	//////////////////////////////2018-11-06_add////////////////////////////////////////
	int		evtForDay;						//日期的日标识,用户重置事件发送次数判定
	int		nChTInterval;	//不变化限制时间间隔,单位秒,默认为0时不执行不刷变更判定
	int     evtNoChangeSendCountForDay;		//每天发送不变化事件次数
	int     evtNoChangeSendCountForLimit;	//每天发送不变化事件限制次数
	int		nUpTInterval;	//不刷新限制时间间隔,单位秒,默认为0时不执行不刷变更判定
	int     evtNoUpdateSendCountForDay;		//每天发送不刷新事件次数
	int     evtNoUpdateSendCountForLimit;	//每天发送不刷新事件限制次数
	EventWay		eway;	//告警方式,主要针对实时校对、定期巡检、控制校验等告警设置,用户定义事件自行配置
};
//设备
struct Dev
{
	DevProPerty			devInfo;
	std::map<unsigned int,PInfo>	pInfo;
};

//设备集
struct GDev
{
	DevGroupProPerty gdev;
	std::map<unsigned int,Dev> devs;
};

}

#endif //CONF_H
