#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STRATEGY_H_
#define _STRATEGY_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : strategy.h
  *File Mark       : 
  *Summary         : 
  *策略类,主要是策略条件判定相关的函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_plan.h"

#include <vector>

using namespace pyfree;

class BusinessDef;  //业务数据集的前置声明,属于单体类,相当于业务数据相关的全局变量+全局函数集

class Strategy
{
protected:
    BusinessDef* pbdef;
private:
    /* data */
public:
    Strategy(/* args */);
    ~Strategy();
protected:
    bool exceptCurTimes(std::vector<ExceptTime> exceptList_, struct tm _tt);
    bool mapCurTimes( std::vector<PlanTime> &_pts);	//定时条件集判定
	bool mapCurTime(PlanTime &_pt);					//定时条件判定
	unsigned int getTVMin(time_t _t);				//
	bool comRangeUIntVal(unsigned int val_, unsigned int valA_, unsigned int valB_);
	bool inWorkTime(unsigned int min_,std::vector<TVProperty> timeVS);	//时间区域判定
	bool mapSleepTime(std::vector<TVProperty> timeVS, PlanSleep &_ps);	//定期条件判定
	//
	bool conditionJude(std::vector<TVProperty> timeVS,std::vector<PlanCondition> _plancons,bool planflag = true); //数值条件判定
	//
	bool mapStartTime(std::vector<TVProperty> timeVS);//启动告警添加时间范围约束
    //
    int getCurTVDay();
};

#endif
