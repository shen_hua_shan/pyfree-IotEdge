#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef VERIFICATION_FOR_CONTROL_THREAD_H
#define VERIFICATION_FOR_CONTROL_THREAD_H
/*
下控结果校验处理线程
*/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"


class VerifyForControlCache;

class VerifyForControl : public acl::thread
{
public:
	VerifyForControl(void);
	~VerifyForControl(void);
	void* run();
protected:
private:
	bool running;
	VerifyForControlCache *ptr_VCC;

};
#endif
