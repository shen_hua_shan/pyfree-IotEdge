# pyfree-sysmgr-master

#### 介绍
实现物联网边缘服务系统的各个子软件（服务)管理的轻量级软件，通过xml配置服务信息，暂时支持本地可视化web终端和阿里云物联网终端界面管理，具有以下几点功能:  
(1)子服务配置文件的上传下载,固件升级  
(2)子服务态势控制与查询  

#### 软件说明  
* 1)总概述：  
    实现win/linux下服务的启动、停止、查询等功能的模块和目录下文件按扩展名查询、文件加载和写入模块，这些功能模块被阿里云物联网接口和web服务接口调用,实现服务管理功能  
	![服务管理软件-结构示意图](/res/for_doc/sysmgr_architecture.PNG)  

  2)类图:  
	![服务管理软件-主要类](/res/for_doc/sysmgr_class.PNG)  

  3)功能描述  
    * 远程启停、升级服务     
	* 远程管理服务配置文件(当前版本仅实现本地web终端,基于阿里云物联网平台管理服务配置文件功能暂未实现)  

#### 编译
##### 一、依赖：
1.  acl_master
acl_master是一个跨平台c/c++库，提供了网络通信库及服务器编程框架，同时提供更多的实用功能库及示例，具体编译与实现请参考其说明文档。  
项目地址: https://gitee.com/acl-dev/acl 或 https://github.com/acl-dev/acl  
技术博客：https://www.iteye.com/blog/user/zsxxsz  

做了两处源码调整:  
由于acl提供的日志记录的时间只到秒级别，本项目需要毫秒的级别，因此修改了一下其源码，  
在lib_acl/src/stdlib/acl_mylog.c文件中，将acl_logtime_fmt函数实现调整为：  
```  
void acl_logtime_fmt(char *buf, size_t size)  
{  
    //time_t	now;  
	struct timeval tm0;  
	gettimeofday(&tm0, NULL);  
	time_t	now = tm0.tv_sec;  
#ifdef	ACL_UNIX  
	struct tm local_time;  

	//(void) time (&now);  
	(void) localtime_r(&now, &local_time);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", &local_time);  
	sprintf(buf, "%s.%03d ", buf,(int)(tm0.tv_usec/1000));  
#elif	defined(ACL_WINDOWS)  
	struct tm *local_time;  

	//(void) time (&now);  
	local_time = localtime(&now);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", local_time);  
	sprintf(buf, "%s.%03d ",buf, (int)(tm0.tv_usec/1000));  
#else  
# error "unknown OS type"  
#endif  
}   
```  
如果不需要毫秒级的时间格式，就不必修改。  
其二:  
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```

2.  iot_aliyun  
iot_aliyun目录是阿里云物联网平台提供的c++_SDK包编译的头文件和库，采用的是iotkit-embedded-2.3.0.zip源码编译的。  
最新版本可以去阿里云物联网平台官网下载，新版本有些函数会有变更，本调度软件涉及阿里云物联网接口部分需要作出调整。 

  
##### 二、项目编译：

> Linux编译
项目编译需要cmake+gcc支持，目前本项目的编译环境是centos7.3，由于需要将git相关信息写入程序,需要安装git  
或在CMakeLists.txt中注销:"execute_process(COMMAND /bin/bash ../build.sh)"  
编译命令类似如下:  
```
cd sysmgr-master  
mkdir build-linux  
cd build-linux  
cmake ..  
make  
```
> win编译
当前win编译测试采用vs2015+cmake编译,例子如下  
备注:若需要32为支持请去掉Win64指定,msbuild为vs的命令编译工具,可以直接vs打开工程文件编译    
```
cd gather-master  
mkdir build-win  
cd build-win  

cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release ..  
msbuild pyfree-sysmgr.sln /p:Configuration="Release" /p:Platform="x64"
或
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Debug ..  
msbuild pyfree-sysmgr.sln /p:Configuration="Debug" /p:Platform="x64"
```

#### demo示例

> Linux  
1. 环境搭建  
安装采集软件、调度软件以及本软件(服务版本),详见:  
demo-project/gather/install_linux.sh  
demo-project/monitor/install_linux.sh  
demo-project/record/install_linux.sh  
demo-project/waringPush/install_linux.sh   
demo-project/sysmgr/install_linux.sh  

2.  程序配置  
程序启动有根据磁盘或网卡校验的License约束  
因此需要向生成License的sn.txt输出文件，启动虚拟机，进入其目录执行指令构建：  
./SWL 0 22 或 ./SWL 1 22  
其会生成sn.txt文件,采集软件在启动时会读取该文件进行License验证  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
3.  项目配置  
appconf.xml：主要配置阿里云物联网平台接口网关设备信息、OTA固件升级信息以及被监控服务配置文件路径  
svc.xml：被监控服务信息，具体看配置文件说明  
```
<!--采集服务和调度服务监控信息配置-->
<SVC id="1" svcname="pyfreeGather" appdir="/usr/local/gather" 
    appname="pyfree-gather.exe" aliyunkey="gather_state" upLoopTime="600" confext="xml;lua"/>
<SVC id="2" svcname="pyfreeMonitor" appdir="/usr/local/monitor" 
    appname="pyfree-monitor.exe" aliyunkey="monitor_state" upLoopTime="600" confext="xml"/>
<SVC id="3" svcname="pyfreeRecord" appdir="/usr/local/record" 
    appname="pyfree-record.exe" aliyunkey="record_state" upLoopTime="600" confext="xml"/>
<SVC id="4" svcname="pyfreeWaringPush" appdir="D:\\workForMy\\pyfree-IotEdge\\demo-project\\waringPush"   
	appname="pyfree-waringPush.exe" aliyunkey="wpush_state" upLoopTime="600" confext="xml"/>  
```
本地web展示采集服务和调度服务被监控示例,本人不对web展示不擅长,展示比较丑陋,有html部署设计的建议请联系我,谢谢      
![服务管理demo界面](../res/for_doc/sysmgr_web.PNG)   
备注:如果非本机web浏览器无法打开web，请确保防火墙没有拦截配置端口  

阿里云物联网平台接口使用简要说明:  
在阿里云接口构建网关，并定义网关功能，例如:  
![阿里物联网平台-网关功能定义服务状态](../res/for_doc/aliyun_svc_func.PNG)  
在appconf.xml配置文件中开启接口功能,并配置网关设备三元组信息:  
```
	<AliyunConf>
		<!--网关设备产品key-->
		<product_key>a1D1cOroRQp</product_key>
		<!--网关设备产品secret-->
		<product_secret>******</product_secret>
		<!--网关设备名-->
		<device_name>pyfree_zh</device_name>
		<!--网关设备secret-->
		<device_secret>******</device_secret>
	</AliyunConf>
```
在svc.xml配置网关设备的功能标识符(aliyunkey):  
```
<SVC id="1" svcname="pyfreeGather" appdir="/usr/local/gather" 
	appname="pyfree-gather.exe" aliyunkey="gather_state" upLoopTime="600" confext="xml;lua"/>
```
管理服务启动后，如果设置开启阿里云物联网平台接口功能,就可以在该网关设备的物模型数据观察被监控服务的实时状态,并可以在在线调试或在数据分析->实验室->空间数据可视化中配置项目实现实时控制(当前阿里物联网平台版本)  
![阿里物联网平台-网关功能数据展示示例](../res/for_doc/aliyun_svc_state.PNG)  

> win64  
1. 环境搭建  

安装采集软件、调度软件以及本软件(服务版本),管理员启动cmd,进入demo-project/sysmgr目录：  
安装:pyfree-sysmgr.exe install  
卸载:pyfree-sysmgr.exe uninstall  
在任务管理器或服务管理页面可以启停服务或配置其服务相关信息  
调度软件和服务管理软件类似  
更新:在任务管理器停止服务,拷贝pyfree-sysmgr.exe覆盖完成更新  

2.  程序配置  
类似linux   
  
3.  项目配置  
类似linux   
 
