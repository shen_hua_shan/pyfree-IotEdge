#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _DATA_DEF_H_
#define _DATA_DEF_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : datadef.h
  *File Mark       : 
  *Summary         : 业务逻辑数据模型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <string>
#include <map>

#include "conf.h"

enum TopicType
{
	TOPIC_DEV_LOGIN			= 1,			//子设备上线 Topic
	TOPIC_DEV_LOGIN_REPLY	= 2,			//子设备上线 Topic_Reply
	TOPIC_DEV_LOGOUT		= 3,			//子设备下线 Topic
	TOPIC_DEV_LOGOUT_REPLY	= 4,			//子设备下线 Topic_Reply
	TOPIC_EVENT_PRO_POST		= 11,		//客户端上送数据 Topic
	TOPIC_EVENT_PRO_POST_REPLY	= 12,		//客户端上送数据 Topic_Reply
	TOPIC_SERVICE_PRO_SET		= 21,		//服务设置属性Topic
	TOPIC_SERVICE_PRO_SET_REPLY = 22,		//
	TOPIC_DEV_CONFIG_GET			= 61,	//设备请求配置Topic
	TOPIC_DEV_CONFIG_GET_REPLY		= 62,	//设备请求配置反馈Topic
	TOPIC_DEV_CONFIG_PUSH			= 63,	//设备推送配置Topic
	TOPIC_DEV_CONFIG_PUSH_REPLY		= 64,	//设备推送配置反馈Topic
	TOPIC_DEFAULT = 0
};

struct AliyunServiceDesc
{
	TopicType topicType;
	std::map<std::string,pyfree::ServiceInfo> triples;
};

enum ServiceState
{
	SVC_STOP = 1,
	SVC_RUN = 2,
	SVC_PAUSE = 3,
	SVC_UNKOWN_STATE = 0
};

struct SvcDesc : public pyfree::ServiceInfo
{
	SvcDesc()
		: ServiceInfo()
		, svc_state(SVC_UNKOWN_STATE)
		, markT(0)
	{};
	SvcDesc(const SvcDesc& rval) : ServiceInfo(rval)

	{
		//子类成员赋值
		svc_state = rval.svc_state;
		markT = rval.markT;
	}
	SvcDesc& operator=(const SvcDesc &rval)
	{
		if (this != &rval) {
			ServiceInfo::operator=(rval);
			svc_state = rval.svc_state;
			markT = rval.markT;
		}
		return *this;
	};
	ServiceState svc_state;
	unsigned int markT;
};
//阿里云物联网平台下发的缓存数据结构
struct RCacheAliyun
{
	RCacheAliyun() 
		: topic(""), payload("")
	{};
	RCacheAliyun(std::string topic_, std::string payload_)
		: topic(topic_)
		, payload(payload_)
	{};
	RCacheAliyun& operator=(const RCacheAliyun &rval)
	{
		if (this != &rval) {
			topic = rval.topic;
			payload = rval.payload;
		}
		return *this;
	};
	std::string topic;
	std::string payload;
};
//推送阿里云物联网平台的缓存数据结构
struct WCacheAliyun
{
	WCacheAliyun() : id(0), val(0)
	{};
	WCacheAliyun(int id_, int val_)
		: id(id_)
		, val(val_)
	{};
	WCacheAliyun& operator=(const WCacheAliyun &rval)
	{
		if (this != &rval) {
			id = rval.id;
			val = rval.val;
		}
		return *this;
	};
	int id;
	int val;
};
//辅助阿里云物联网平台固件升级的数据结构
struct OTAAliyunCache : public pyfree::OTAAliyun
{
	OTAAliyunCache()
		: OTAAliyun()
		, updatef(false)
		, appDir("")
		, batfile("ExportZip.bat")
		, pathdiv("")
	{

	};
	void form(const pyfree::OTAAliyun &rval)
	{
		version_file 	= rval.version_file;
		update_file 	= rval.update_file;
		zip_path 		= rval.zip_path;
		update_dir 		= rval.update_dir;
	};
	bool updatef;
	std::string appDir;
	std::string batfile;
	std::string pathdiv;
};

#endif
