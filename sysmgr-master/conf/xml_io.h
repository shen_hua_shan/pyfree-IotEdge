#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : xml_io.h
  *File Mark       : 
  *Summary         : 业务配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifndef _XML_IO_H_
#define _XML_IO_H_

#include <map>

#include "conf.h"

namespace pyfree
{
  /**
	 * 从配置文件读取程序运行参数
   * @param conf {ServiceConf& } 程序运行参数集
   * @param xml_ {string } 配置文件
	 * @return {void}
	 */
	void readAppConf(ServiceConf &conf, std::string xml_ = "appconf.xml");
  /**
	 * 向配置文件写入程序默认运行参数,待实现
   * @param conf {ServiceConf } 程序运行参数集
   * @param xml_ {string } 配置文件
	 * @return {void}
	 */
	void writeAppConf(const ServiceConf conf, std::string xml_ = "appconf.xml");
  /**
	 * 从配置文件读取服务配置相关信息
   * @param svcmaps {map& } 服务配置信息
   * @param xml_ {string } 配置文件
	 * @return {void}
	 */
	void readSvcMaps(std::map<int,ServiceInfo> &svcmaps, std::string xml_ = "svc.xml");
};

#endif
