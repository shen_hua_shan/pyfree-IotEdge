#ifndef _DATA_TYPE_DEF_H_
#define _DATA_TYPE_DEF_H_
/***********************************************************************
  *Copyright 2020-06-13, pyfree
  *
  *File Name       : dtypedef.h
  *File Mark       : 
  *Summary         : 自定义数据类型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
namespace pyfree{
//告警方式
enum EventWay
{
	AlarmForLog = 1,		//日志
	AlarmForSMS = 2,		//短信
	AlarmForEMail = 3,		//邮件
	AlarmForDef = 0
};
}

#endif
