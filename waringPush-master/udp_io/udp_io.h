#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _UDP_IO_H_
#define _UDP_IO_H_
/***********************************************************************
  *Copyright 2020-06-12, pyfree
  *
  *File Name       : udp_io.h
  *File Mark       :
  *Summary         : 
  *告警信息通信接口实现,采用udp方式通信,作为接收方,单向接收来自调度软件推送的告警信息
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"
#include "acl_cpp/stdlib/json.hpp"

#include "dtypedef.h"
#include "conf_war.h"
#include "queuedata_single.h"

class MsgToAliyun;
class Email;
class PushThread;

class UdpIO : public acl::thread
{
private:
    /* data */
    bool running;		    //线程运行标记
    std::string local_addr; //
    std::string peer_addr; //
    //
    QueueDataSingle<EventForWaring> *queueforwar;
	MsgToAliyun *msg_to_ali_ptr;
	Email* email_ptr;
    //
    bool msgFlag;
	bool mailFlag;
    //
    std::string ControlCheckKey;
    //
    PushThread *ptr_push;               //从缓存告警队列中读取数据更新发布
public:
    UdpIO(bool msg_,bool mail_,std::string addr="127.0.0.1",int port=60005);
    ~UdpIO();

    void* run();
    //
    void push();
private:
    int AddFrameDef(const unsigned char *pBuf, int len);
    bool toJSon(acl::json &json,const unsigned char *pBuf, int len);
    bool checkCode(EventForWaring efw);
};

#endif
